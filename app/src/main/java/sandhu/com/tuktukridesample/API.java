package sandhu.com.tuktukridesample;


import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;


public interface API {

    @FormUrlEncoded
    @POST("/tuktuk/upload_location.php")
    Call<LocationUpdateResponse> updateLocationOnServer(@Field("lat") String lat,
                                                        @Field("lon") String lon,
                                                        @Field("area") String area);
}

