package sandhu.com.tuktukridesample;

import android.app.Activity;
import android.app.ActivityManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends Activity {

    private static final int REQUEST_PERMISSIONS = 1010;
    boolean boolean_permission;
    TextView tv_latitude, tv_longitude, tv_address, tv_area, tv_locality;
    Double latitude, longitude;
    Geocoder geocoder;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        tv_address = findViewById(R.id.tv_address);
        tv_latitude = (TextView) findViewById(R.id.tv_latitude);
        tv_longitude = (TextView) findViewById(R.id.tv_longitude);
        tv_area = (TextView) findViewById(R.id.tv_area);
        tv_locality = (TextView) findViewById(R.id.tv_locality);
        geocoder = new Geocoder(this, Locale.getDefault());
        checkPermission();
        startListingLocation();
    }

    private void startListingLocation() {
        // TODO: 2/2/2019 WE CAN CHANGE THE FREQUENCY OF GETTING USER LOCATION FROM SERVICE ,
        // CURRENTLY IT IS GETTING LOCATION EVERY 5 SECOND.
        if (boolean_permission) {
            if (!isMyServiceRunning(FetchLocationService.class)) {
                Intent intent = new Intent(getApplicationContext(), FetchLocationService.class);
                startService(intent);

            } else {
                Toast.makeText(getApplicationContext(), "Service is already running", Toast.LENGTH_SHORT).show();
            }
        } else {
            checkPermission();
        }

    }

    private void checkPermission() {
        if ((ContextCompat.checkSelfPermission(getApplicationContext(), android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED)) {
            ActivityCompat.requestPermissions(MainActivity.this, new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION},
                    REQUEST_PERMISSIONS);
        } else {
            boolean_permission = true;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        switch (requestCode) {
            case REQUEST_PERMISSIONS: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    boolean_permission = true;
                    startListingLocation();
                } else {
                    boolean_permission = false;
                    Toast.makeText(getApplicationContext(), "Please allow the permission", Toast.LENGTH_LONG).show();

                }
            }
        }
    }

    private BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            latitude = Double.valueOf(intent.getStringExtra("latitude"));
            longitude = Double.valueOf(intent.getStringExtra("longitude"));

            List<Address> addresses = null;

            try {
                addresses = geocoder.getFromLocation(latitude, longitude, 1);
                String cityName = addresses.get(0).getAddressLine(0);
                String stateName = addresses.get(0).getAddressLine(1);
                String countryName = addresses.get(0).getAddressLine(2);

                tv_area.setText(addresses.get(0).getAdminArea());
                tv_locality.setText(stateName);
                tv_address.setText(countryName);
                sendLocationToTukTukServer(addresses.get(0).getAdminArea(), latitude, longitude);
            } catch (IOException e1) {
                e1.printStackTrace();
            }

            tv_latitude.setText(String.format("Current Latitude: %s", latitude + ""));
            tv_longitude.setText(String.format("Current Longitude: %s", longitude + ""));


        }
    };

    private void sendLocationToTukTukServer(String adminArea, Double latitude, Double longitude) {
        API api = RestClient.getClient().create(API.class);
        api.updateLocationOnServer(String.valueOf(latitude), String.valueOf(longitude), adminArea).enqueue(new Callback<LocationUpdateResponse>() {
            @Override
            public void onResponse(Call<LocationUpdateResponse> call, Response<LocationUpdateResponse> response) {
                if (response.isSuccessful() && response.body() != null) {
                    Log.d("server_response", response.body().getMessage());
                }
            }

            @Override
            public void onFailure(Call<LocationUpdateResponse> call, Throwable t) {
                Log.d("connection_problem", "will write code to tackle internet connection and other problem");
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        registerReceiver(broadcastReceiver, new IntentFilter(FetchLocationService.str_receiver));

    }

    @Override
    protected void onPause() {
        super.onPause();
        unregisterReceiver(broadcastReceiver);
    }

    private boolean isMyServiceRunning(Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }
}